# blender_mathematics

The supporting repo for youtube series on blender mathematics: https://youtu.be/Za1dEvyHSag

All the scripts used in a particular video are placed inside the correspondent folder.

All the scripts are MIT Licensed.
